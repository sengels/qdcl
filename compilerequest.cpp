#include "compilerequest.h"

#include <QtNetwork/QLocalSocket>
#include <QtCore/QSharedMemory>
#include <QtCore/QDataStream>
#include <QtCore/QByteArray>
#include <QtCore/QBuffer>
#include <QtCore/QDebug>

#include "common.h"

CompileRequest::CompileRequest(const QString& server)
 : m_server(server)
 , m_localSocket(new QLocalSocket)
 , m_sharedMemory(new QSharedMemory) {
}

bool CompileRequest::request(QString key, QString command, const QByteArray& content) {
    qDebug() << "request key:" << key;
    if(m_localSocket->state() != QLocalSocket::ConnectedState || !m_localSocket->isValid()) {
        return false;
    }

    m_sharedMemory->setKey(key);
    m_sharedMemory->create(content.size());

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::RequestCompileJob;
    out << (quint64)content.size();
    out << key;
    out << command;
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));

    m_sharedMemory->lock();
    char *to = (char*)m_sharedMemory->data();
    const char *from = content.data();

    if(m_sharedMemory->size() < content.size()) {
        m_sharedMemory->unlock();
        return false;
    }
    memcpy(to, from, content.size());
    m_sharedMemory->unlock();

    const int len = m_localSocket->write(block);
    qDebug() << "send to local socket package of length:" << len;
    return true;
}

bool CompileRequest::waitForAnswer() {
    if(!m_localSocket->isValid()) return false;

    const bool failOnReadyRead = 
    m_localSocket->waitForReadyRead(1000);
    qDebug() << "wait for a reply:" << failOnReadyRead;

    QDataStream in(m_localSocket);
    in.setVersion(QDataStream::Qt_4_0);

    while(m_localSocket->bytesAvailable() < (qint64)sizeof(qint64)) {
        qDebug() << "qdcl not enough bytes available for blockSize" << m_localSocket->bytesAvailable();
        // FIXME: make these timeouts adjustable
        m_localSocket->waitForReadyRead(100);
    }

    qint64 blockSize;
    in >> blockSize;

    while(m_localSocket->bytesAvailable() < blockSize) {
        qDebug() << "qdcl not enough bytes available for complete block:" << blockSize << m_localSocket->bytesAvailable();
        // FIXME: make these timeouts adjustable
        m_localSocket->waitForReadyRead(100);
    }

    quint16 magic;
    in >> magic;

    qDebug() << "qdcl received magic:" << magic;
    
    switch(magic) {
        case Magix::FinishJob:
            {
                quint32 errorOutputSize;
                quint32 contentSize;
                QString key;
                in >> key;
                in >> m_exitCode;
                in >> errorOutputSize;
                in >> contentSize;
                qDebug() << "qdcl MarkCompileJobFinished:" << m_exitCode << errorOutputSize << contentSize << "!";

                m_sharedMemory->setKey(key);
                m_sharedMemory->attach();
                m_sharedMemory->lock();

                QBuffer buffer;
                QDataStream in(&buffer);

                buffer.setData((char*)m_sharedMemory->constData(), m_sharedMemory->size());
                buffer.open(QBuffer::ReadOnly);
                m_errorOutput = buffer.read(errorOutputSize);
                m_output = buffer.read(contentSize);
                buffer.close();

                m_sharedMemory->unlock();
                m_sharedMemory->detach();

                m_errorOutput = qUncompress(m_errorOutput);
                m_output = qUncompress(m_output);
                break;
            }
        default:
            {
                qDebug() << "qdcl couldn't find this magic:" << magic;
            }
    }
    return true;
}

bool CompileRequest::connect() {
    m_localSocket->connectToServer(m_server);
    if(!m_localSocket->waitForConnected())
        return false;
    return true;
}

const QByteArray& CompileRequest::output() const {
    return m_output;
}

const QByteArray& CompileRequest::error() const {
    return m_errorOutput;
}

quint16 CompileRequest::exitCode() const {
    return m_exitCode;
}
