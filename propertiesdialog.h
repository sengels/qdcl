#ifndef propertiesdialog_h
#define propertiesdialog_h

#include <QtCore/QList>
#include <QtCore/QSettings>
#include <QtCore/QStringList>
#include <QtGui/QWidget>

class QCheckBox;
class QLabel;
class QLineEdit;
class QDialogButtonBox;

/** This dialog sets all the required properties for qdcl and saves them into the registry
 */
class PropertiesDialog : public QWidget {
    Q_OBJECT
    public:
        PropertiesDialog(QWidget *parent = 0);
        ~PropertiesDialog();
        
    public slots:
        void applySettings();
        void resetSettings();

    private:
        QStringList         m_keys;
        QList<QLineEdit*>   m_lineEdits;
        QList<QLabel*>      m_labels;
        QDialogButtonBox*   m_buttons;
        
        QSettings           m_settings;
};
#endif /* propertiesdialog_h */
