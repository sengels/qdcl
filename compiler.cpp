#include "compiler.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QProcess>
#include <QtCore/QVariant>
#include <QtNetwork/QHostInfo>

#include <windows.h>
#include <process.h>

#include "common.h"
#include "commandlineparser.h"
#include "compilerequest.h"

Compiler::Compiler()
 : m_logFileName()
 , m_settings("qdcl", "qdcl")
 , m_parser(0)
{
    m_logFileName = m_settings.value("logfilename", "C:/qdcl/tmp/qdcl.log").toString();
    
    m_logFile.setFileName(m_logFileName);
    if(false && m_logFile.open(QIODevice::ReadWrite)) {
        m_logStream.setDevice(&m_logFile);
    } else {
        m_logFile.open(stdout, QIODevice::ReadOnly);
        m_logStream.setDevice(&m_logFile);
    }
    
}

bool Compiler::parseParameters(QStringList params) {
    m_parser = new CommandlineParser(params);
    return m_parser->parse();
}

QString& Compiler::logFileName()
{
    return m_logFileName;
}

bool Compiler::preprocess()
{
//    qDebug() << "preprocess arguments:" << m_preprocessParams;
    QProcess process;

    process.start(compilerPath() + " /E " + m_parser->preprocessResult().join(" ") + " " + m_parser->sourceFile());

    // there is a timeout here, maybe we want to use it later?
    if (!process.waitForFinished())
        return false;

    m_content = qCompress(process.readAll(), -1);
//    qDebug() << "got output:" << m_content.size();
    if(process.exitCode() == 0) return true;
    qDebug() << "Warning, an error occured with output:" << m_content;
    return false;
}

int Compiler::compile()
{
    if(!m_parser->fallback() && preprocess()) {
        // first create the shared memory
        QString key = QString("%1-%2-%3").arg(QHostInfo::localHostName()).arg(qHash(m_parser->sourceFile())).arg(_getpid());
        CompileRequest request("qdcl");
        if(!request.connect()) {
            qDebug() << "connection to server for Request failed!";
            return fallbackCompile();
        }
        if(!request.request(key, m_parser->compileResult().join(" "), m_content) || 
           !request.waitForAnswer()) {
            qDebug() << "request or answer failed!";
            return fallbackCompile();
        }

        fprintf(stderr, request.error().data());
        QFile outfile(m_parser->objectFile());
        outfile.open(QIODevice::ReadWrite);
        outfile.write(request.output());
        outfile.close();

        qDebug() << "finished with exit code:" << request.exitCode() << "\r\n";
        return request.exitCode();
    } else {
        return fallbackCompile();
    }
}

inline QString Compiler::compilerPath() const {
    return "\"" + QString(qgetenv("VCINSTALLDIR")) + "\\bin\\cl.exe\"";
}

int Compiler::fallbackCompile()
{
    qDebug() << "fallback!";
    QProcess proc;
    proc.start(compilerPath() + " " + m_parser->fallbackResult().join(" "));
    proc.waitForFinished();
    int ret = proc.exitCode();
    fprintf(stdout, proc.readAllStandardOutput());
    fprintf(stderr, proc.readAllStandardError());
    return ret;
}
