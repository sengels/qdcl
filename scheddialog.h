#ifndef _scheddialog_h
#define _scheddialog_h

#include <QtCore/QString>
#include <QtGui/QDialog>

class QTcpServer;
class QComboBox;
class QLayout;

class Visualizer;
class ConnectWidget;
class ClientManager;
class SchedulerLocator;
class CompileManager;
class LoadManager;

enum ServerMode {
    Scheduler,
    Slave
};

/**
  * The Server class is the main widget for the Application. It handles the current mode of the application.
  * FIXME: add some more helper functions e.g. for the netmask and the local broadcast address
  * FIXME: rename this class to something that says more about what this is
  */
class Server : public QDialog
{
    Q_OBJECT

public:
    Server(QWidget *parent = 0);

    QString ip();
    quint16 port();

    QString ownIp() const;
public slots:
    void connectServer();
    void disconnectServer();

    void setIp(const QString& ip);
    void setPort(quint16 port);
    void setPortString(const QString& port);
    void switchWidgets(int mode);

signals:
    void modeChanged(int mode);
    void ipChanged(QString ip);
    void portChanged(quint16 port);

private slots:
    
    void setHost(const QString& ip, quint16 port);

private:
    ServerMode          m_serverMode;

    QString             m_ip;
    quint16             m_port;

    QComboBox*          m_comboBox;
    QLayout*            m_layout;

    Visualizer*         m_visualizer;
    ConnectWidget*      m_connectWidget;
    ClientManager*      m_clientManager;
    SchedulerLocator*   m_locator;
    CompileManager*     m_compileManager;
    LoadManager*        m_loadManager;
};
#endif