#ifndef _compilerequest_h
#define _compilerequest_h

#include <QtCore/QPointer>
#include <QtCore/QString>
#include <QtCore/QByteArray>

class QLocalSocket;
class QSharedMemory;

class CompileRequest {
    public:
        CompileRequest(const QString& server);

        bool request(QString key, QString command, const QByteArray& content);
        bool waitForAnswer();
        bool connect();

        const QByteArray& output() const;
        const QByteArray& error() const;
        quint16 exitCode() const;

    private:
        QString m_server;
        QPointer<QLocalSocket>  m_localSocket;
        QPointer<QSharedMemory> m_sharedMemory;
        QByteArray              m_output;
        QByteArray              m_errorOutput;
        quint16                 m_exitCode;
};
#endif /* _compilerequest_h */
