#ifndef _schedulerlocator_h
#define _schedulerlocator_h

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>

class QUdpSocket;
class Server;

/**
  * This class is used to find or announce a running scheduler.
  * it handles both Client and SchedulerMode.
  */
class SchedulerLocator : public QObject {
    Q_OBJECT
    public:
        SchedulerLocator(Server* server);
        bool schedulerMode();

    public slots:
        void announceScheduler();
        void requestScheduler();
        // FIXME: base this on a method of Server and not duplicate the information locally
        void switchSchedulerMode();

    private slots:
        void receiveSchedulerInfo();
        void outputError(QAbstractSocket::SocketError socketError);

    signals:
        /**
          * this signal is emitted when a scheduler has announced its existence 
          * (e.g. it has replied to our request)
          */
        void schedulerReceived();

        /**
          * this signal is emitted when some other computer on the network has requested
          * a scheduler. This can be ignored in case we're no scheduler ourselves
          */
        void schedulerRequested();

        /**
          * this signal is emitted after we have received a scheduler.
          * @p ip is the ip of the scheduler
          * @p port is the port on which the scheduler is listening to incoming connections
          */
        void schedulerFound(QString ip, quint16 port);

    private:
        QPointer<QUdpSocket>    m_udpSocket;
        QPointer<Server>        m_server;
        QHostAddress            m_broadcast;
        quint16                 m_locatorPort;
        bool                    m_schedulerMode;
};
#endif