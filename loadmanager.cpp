#include "loadmanager.h"

#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtCore/QByteArray>
#include <QtCore/QDataStream>
#include <QtCore/QFile>
#include <QtNetwork/QLocalServer>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QHostInfo>

#include "common.h"
#include "scheddialog.h"
#include "load.h"

// public:
LoadManager::LoadManager(Server* server)
    : m_tcpSocket(0),
      m_localPort(34567),
      m_connected(false),
      m_tokenNumber(QThread::idealThreadCount()),
      m_localServer(0),
      m_blockSize(0),
      m_server(server)
{
    m_tcpSocket = new QTcpSocket(this);
    m_localServer = new QLocalServer(this);
    
    connect(m_localServer, SIGNAL(newConnection()), this, SLOT(addNewLocalRequest()));
    const bool listenServer = 
    m_localServer->listen("qdcl");
//    qDebug() << "listening on localServer:" << listenServer;
}

quint16 LoadManager::tokenNumber() const
{
    return m_tokenNumber;
}

bool LoadManager::connectStatus() const
{
    return m_connected;
}

// public slots:
void LoadManager::addNewLocalRequest()
{
    m_waitLoads.enqueue(new Load);
    qDebug() << "adding new Local Request!" << m_waitLoads.last();
    connect(m_waitLoads.last(), SIGNAL(jobRequested()), this, SLOT(requestJob()));
    connect(m_waitLoads.last(), SIGNAL(terminated()), this, SLOT(finishJob()));
    m_waitLoads.last()->setLocalSocket(m_localServer->nextPendingConnection());
}

void LoadManager::checkConnectStatus()
{
    if(m_tcpSocket->isValid()) {
        m_connected = true;
        emit connectStatusChanged(m_connected);
   }
}

void LoadManager::checkDisconnectStatus()
{
    m_connected = false;
    emit connectStatusChanged(m_connected);
}

void LoadManager::connectDefault()
{
    disconnect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(receiveData()));
    disconnect(m_tcpSocket, SIGNAL(connected()), this, SLOT(checkConnectStatus()));
    disconnect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(checkDisconnectStatus()));
    m_tcpSocket->abort();
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(receiveData()));
    connect(m_tcpSocket, SIGNAL(connected()), this, SLOT(checkConnectStatus()));
    connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(checkDisconnectStatus()));
    m_tcpSocket->connectToHost(m_server->ip(), m_server->port());
}

void LoadManager::disconnectServer()
{
    m_tcpSocket->abort();
    m_tcpSocket->close();
    disconnect(m_tcpSocket, SIGNAL(connected()), this, SLOT(checkConnectStatus()));
    disconnect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(checkDisconnectStatus()));
    m_connected = false;
    emit connectStatusChanged(m_connected);
}

void LoadManager::sendSlaveInformations()
{
//    qDebug() << "TokenNumber:" << m_tokenNumber;
//    qDebug() << "Hostname:" << QHostInfo::localHostName();

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::SendSlaveInfo;
    out << m_tokenNumber;
    out << QHostInfo::localHostName();
    out << m_localPort;
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));

    m_tcpSocket->write(block);
}

void LoadManager::requestJob()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::RequestJob;
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));
    
    qDebug() << "sending job request to:" << m_tcpSocket->peerAddress() << m_tcpSocket->peerPort();
    const int len =
    m_tcpSocket->write(block);
    qDebug() << "sending requestJob with len" << len;
}

void LoadManager::finishJob()
{
    Load *load = qobject_cast<Load*>(sender());
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::FinishJob;
    out << (quint64)load->id();
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));
    const int bytesWritten = 
    m_tcpSocket->write(block);
//    qDebug() << "writing that many bytes:" << bytesWritten << "to:" << m_tcpSocket->peerAddress() << m_tcpSocket->peerPort();
    bool jobRemoved = 
    m_waitLoads.removeOne(load);
    jobRemoved |=
    m_loads.removeOne(load);
    qDebug() << "removing terminated job:" << jobRemoved << "with id:" << load->id();
    delete load;
}

void LoadManager::dumpCurrentStream()
{
    char dump[1024];
    int len = m_tcpSocket->peek(dump, 1024);
    QString test;
    for(int i = 0; i <= len; i++) {
        test.append(QString(" %1").arg((int)dump[i]));
    }
    qDebug() << test;
}

void LoadManager::receiveData()
{
    QDataStream in(m_tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_tcpSocket->bytesAvailable() < (qint64)sizeof(qint64)) {
        return;
    }

//    dumpCurrentStream();

    in >> m_blockSize;
    
//    qDebug() << "blockSize:" << m_blockSize;

    if (m_tcpSocket->bytesAvailable() < m_blockSize) {
        return;
    }
//    dumpCurrentStream();

    quint16 magic;
    QByteArray data;

    in >> magic;

    switch(magic) {
        case Magix::AnswerRequestJob:
            {
                QString address;
                QString key;
                int port;
                int status;
                quint64 id;

//                qDebug() << "answering job request!";
                in >> address;
                in >> port;
                in >> status;
                in >> id;
                m_loads.enqueue(m_waitLoads.dequeue());
                m_loads.last()->setRemoteInfo(address, port, id);
                connect(m_loads.last(), SIGNAL(jobRequested()), m_loads.last(), SLOT(start()));
                break;
            }
        case Magix::RequestSlaveInfo:
            {
//                qDebug() << "answer with slave info";
                sendSlaveInformations();
                break;
            }
        default:
            {
                qDebug() << "couldn't find this magic:" << magic;
            }
    }
    m_blockSize = 0;
}



#include "loadmanager.moc"