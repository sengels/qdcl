#include "schedulerlocator.h"

#include <QtCore/QByteArray>
#include <QtCore/QDebug>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtNetwork/QUdpSocket>

#include "scheddialog.h"

SchedulerLocator::SchedulerLocator(Server* server)
 : m_udpSocket(0)
 , m_locatorPort(21182)
 , m_schedulerMode(false)
 , m_server(server)
{
    // FIXME: replace with use of Server::ownIp()
    m_broadcast = QHostAddress("192.168.2.255");
    m_udpSocket = new QUdpSocket(this);
    m_udpSocket->bind(m_locatorPort);

    connect(m_udpSocket, SIGNAL(readyRead()),
            this, SLOT(receiveSchedulerInfo()));
    connect(m_udpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(outputError(QAbstractSocket::SocketError)));
    requestScheduler();
}

void SchedulerLocator::announceScheduler()
{
    QByteArray datagram = QString("announceScheduler:%1:%2").arg(m_server->ip()).arg(m_server->port()).toAscii();
    const int datagramSize = m_udpSocket->writeDatagram(datagram.data(), datagram.size(), m_broadcast, m_locatorPort);
//    qDebug() << "wrote Datagram of size" << datagramSize << "to port" << m_locatorPort;
}

void SchedulerLocator::outputError(QAbstractSocket::SocketError socketError)
{
    qDebug() << "schedulerlocator error:" << ((int)socketError);
}

void SchedulerLocator::switchSchedulerMode()
{
    m_schedulerMode = !m_schedulerMode;
    if(m_schedulerMode) {
        connect(this, SIGNAL(schedulerRequested()), this, SLOT(announceScheduler()));
    } else {
        disconnect(this, SIGNAL(schedulerRequested()), this, SLOT(announceScheduler()));
    }
}

bool SchedulerLocator::schedulerMode()
{
    return m_schedulerMode;
}

void SchedulerLocator::requestScheduler()
{
    QByteArray datagram = "requestScheduler";
    const int datagramSize = m_udpSocket->writeDatagram(datagram.data(), datagram.size(), m_broadcast, m_locatorPort);
//    qDebug() << "request length:" << datagramSize << m_locatorPort;
}

void SchedulerLocator::receiveSchedulerInfo()
{
//    qDebug() << "processing pending datagrams:";
    while (m_udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(m_udpSocket->pendingDatagramSize());
        m_udpSocket->readDatagram(datagram.data(), datagram.size());
//        qDebug() << "datagram:" << datagram;

        if(m_schedulerMode && datagram == "requestScheduler") {
            announceScheduler();
            //emit schedulerRequested();
            continue;
        } else {
//            qDebug() << "somebody else requested a scheduler, but we're no scheduler ourselves";
        }

        if(!m_schedulerMode && datagram.startsWith("announceScheduler:")) {
            datagram.replace("announceScheduler:", "");
//            qDebug() << "found scheduler at" << datagram;

            QList<QByteArray> ipPortBlock = datagram.split(':');
            if(ipPortBlock.length() != 2) continue;
            emit schedulerFound(ipPortBlock[0], ipPortBlock[1].toUInt());
        } else {
//            qDebug() << "we're either in schedulerMode" << m_schedulerMode << "or datagram is broken:" << datagram;
        }
    }
}

#include "schedulerlocator.moc"