#ifndef _client_h
#define _client_h

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QString>

class Job;
class QTcpSocket;
class ClientManager;

/**
  * This class represents a client on the scheduler (both local and remote).
  * It interacts with the Load objects of this client via tcp and keeps
  * track of the load on the scheduler site. It also should be the data class
  * on the scheduler site per client.
  */
class Client : public QObject {
    Q_OBJECT

    public:
        Client(ClientManager *man, QTcpSocket *conn);
        
        virtual QString address() const;
        virtual quint16 port() const;
        virtual QString name() const;
        
        virtual int coreNumber() const;
        virtual bool containsId(quint64 id) const;
        virtual Job* getJobForId(quint64 id);
    signals:
        void clientDisconnected();
        void informationChanged();
        void jobRequested();
        void jobFinished(Job *job);

    public slots:
        virtual void read();
        virtual void requestInfo();

        virtual void addJob(Job *job);
        virtual void removeJob(Job *job);
        /**
          * status - whether or not we can accept this request now
          *          a non zero status means dismiss for now
          */
        virtual void answerRequestJob(Job *job, const QString& address, int port);
    protected:
        Client(int localport, int coreNum);

        QPointer<ClientManager> m_manager;
        QPointer<QTcpSocket>    m_conn;
        QString                 m_hostName;
        QList<QPointer<Job>>    m_jobs;
        int                     m_port;
        int                     m_coreNumber;
        qint64                  m_blockSize;
};
#endif