#ifndef _localclient_h
#define _localclient_h

#include "client.h"
#include "scheddialog.h"

/**
  * The LocalClient is the client that builds directly on the server
  * instead of communication via tcp
  */
class LocalClient : public Client {
    Q_OBJECT
    public:
        LocalClient(Server* server);

        QString name() const;
        QString address() const;
        
    private:
        QPointer<Server>    m_server;
};
#endif