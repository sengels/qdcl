#ifndef _clientobject_h
#define _clientobject_h

#include <QtCore/QObject>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>

class QTcpSocket;
class QUdpSocket;

class _Client : public QObject
{
    Q_OBJECT

public:
    _Client();

public slots:
    void requestNewFortune();

private slots:
    void readFortune();
    void displayError(QAbstractSocket::SocketError socketError);

private:
    QTcpSocket*     m_tcpSocket;
    QUdpSocket*     m_udpSocket;
    QHostAddress    m_broadcast;
    QString         m_currentFortune;
    quint16         m_blockSize;
};

#endif