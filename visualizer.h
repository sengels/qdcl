#ifndef _visualizer_h
#define _visualizer_h

#include <QtGui/QWidget>

class QLabel;
class ClientManager;
class QTableWidget;

/**
* The Visualizer class shows the information taken from the clientmanager about the currently connected 
* clients and their workload
*/
class Visualizer : public QWidget {
    Q_OBJECT
    public:
        Visualizer(ClientManager *man, QWidget *parent = 0);
        
        QSize minimumSizeHint() const;
    public slots:
        void addEntry(int line);
        void updateEntry(int line);
        void removeEntry(int line);
        
    private:
        ClientManager *m_manager;
        QLabel *m_label;
        QTableWidget *m_tableWidget;
};
#endif