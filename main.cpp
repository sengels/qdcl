#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QStringList>

#include <windows.h>

#include "compiler.h"

void messageHandler(QtMsgType type, const char *msg)
{
    static QFile* fileptr = new QFile("C:/qdcl/qdcl.log");
    fileptr->open(QIODevice::Append);
    switch (type) {
    case QtDebugMsg:
//         fprintf(stderr, "Debug: %s\n", msg);
        OutputDebugString(TEXT(msg));
        break;
    case QtWarningMsg:
//        fprintf(stderr, "Warning: %s\n", msg);
        fileptr->write(msg);
        fileptr->write("\r\n");
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }
    fileptr->close();
}

int main(int argc, char**argv)
{
    qInstallMsgHandler(messageHandler);

    QCoreApplication app(argc, argv);
    Compiler comp;
    qWarning() << "application arguments:" << app.arguments().join(" ");
    if(!comp.parseParameters(app.arguments())) {
        qWarning() << "parameter parsing failed!";
        return 1;
    } else {
        return comp.compile();
    }
}