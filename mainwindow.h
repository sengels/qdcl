#ifndef mainwindow_h
#define mainwindow_h

#include <QtGui/QWidget>

class QSystemTrayIcon;
class PropertiesDialog;

class MainWindow : public QWidget {
    Q_OBJECT
    public:
        MainWindow(QWidget *parent = 0);
        ~MainWindow();
        
    public slots:
    private:
        QSystemTrayIcon*    m_trayIcon;
        PropertiesDialog*   m_propDialog;
};

#endif /* mainwindow_h */