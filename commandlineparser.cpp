#include "commandlineparser.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>

CommandlineParser::CommandlineParser(QStringList params)
 : m_parameters(params)
 , m_fallback(false) {
}
bool CommandlineParser::parse() {
    if(m_parameters.size() < 1) {
        qDebug() << "not enough parameters (parameter list is empty or broken)!";
        return false;
    }

    QString basename = QFileInfo(m_parameters[0]).baseName();
    m_parameters.removeFirst();
    m_fallbackParams = m_parameters;

    if(basename == QString("qdcl") || basename == QString("cl")) {
        return parseMSVCParameters(m_parameters);
    } else {
        qDebug() << "there is no implementation for this compiler name:" << QFileInfo(m_parameters[0]).baseName();
        return false;
    }
}

bool CommandlineParser::fallback() const {
    return m_fallback;
}

QString CommandlineParser::sourceFile() const {
    return m_sourceFileName;
}

QString CommandlineParser::objectFile() const {
    return m_outputName;
}

QString CommandlineParser::debugFile() const {
    return m_debugName;
}

QStringList CommandlineParser::preprocessResult() const {
    return m_preprocessParams;
}

QStringList CommandlineParser::fallbackResult() const {
    return m_fallbackParams;
}

QStringList CommandlineParser::compileResult() const {
    return m_compileParams;
}

QStringList CommandlineParser::resolveAtSyntax(const QStringList& param)
{
    QStringList retList;
    for(int i = 0; i < param.size(); ++i) {
        if(param[i].startsWith("@")) {
//            qDebug() << "opening file" << param[i].right(param[i].length() - 1);
            QFile tmpfile(param[i].right(param[i].length() - 1));
            tmpfile.open(QIODevice::ReadOnly);
            retList.append(QString(tmpfile.readLine()).replace("\r", "").replace("\n", "").split(' '));
            tmpfile.close();
        } else {
            retList << param[i];
        }
    }
    
    return retList;
}

bool CommandlineParser::parseMSVCParameters(QStringList param)
{
    if(param.size() == 0) {
        qDebug() << "no parameters!";
        m_fallback = true;
        return true;
    }
    
    param = resolveAtSyntax(param);
    qWarning() << "complete parameters:" << param;
    
    QStringList fallbackFails;
    fallbackFails << "/?" << "/help" << "/link" << "/E" << "/P" << "/EP";
    foreach(const QString& f, fallbackFails) fallbackFails << (QString("-") + f.right(f.length() - 1));

    // we must check that this compile step is really meant to output only
    // an object file - if that is not the case, the compiler will fall back
    // to and generate an exe file.
    bool foundCompileObject = false;
    QStringList::ConstIterator it = param.constBegin();
    const QStringList::ConstIterator end = param.constEnd();
    QString arg = *it;
    for(; it != end; it++) {
        arg = *it;
        if(arg.isEmpty()) continue;

        // check whether this is one of the arguments we can directly fallback on
        if(fallbackFails.contains(arg)) {
//            qDebug() << "fallbackFail:" << arg;
            m_fallback = true;
            return true;
        }
        
        // expect this to be an include command
        if(arg.startsWith("/I") || arg.startsWith("-I")) {
            if(arg.length() == 2) {
                // expect that the real argument is in the next arg
                it++; if(it != end) arg = *it; else { m_fallback = true; return true; }
                m_preprocessParams << "/I" << arg;
            } else {
                m_preprocessParams << arg;
            }
//            qDebug() << "found include";
            continue;
        }
        
        if(arg.startsWith("/O") || arg.startsWith("-O")) {
//            qDebug() << "found optimization";
            m_preprocessParams << arg;
            m_compileParams << arg;
            continue;
        }
        
        // fallback on precompiled headers and executables
        if(arg.startsWith("/Fp") || arg.startsWith("/Fe") ||
           arg.startsWith("-Fp") || arg.startsWith("-Fe")) {
//            qDebug() << "fallback on precompiled headers and executables";
            m_fallback = true;
            return true;
        }
        
        // the program database
        if(arg.startsWith("/Fd") || arg.startsWith("-Fd")) {
            m_debugName = arg.right(arg.length() - 3);
            continue;
        }
        
        // handle the object output filename
        if(arg.startsWith("/Fo") || arg.startsWith("-Fo")) {
            m_outputName = arg.right(arg.length() - 3);
//            qDebug() << "m_outputName:" << m_outputName;
            continue;
        }
        
        // handle the compile object file option
        if(arg == "/c" || arg == "-c") {
//            qDebug() << "found compile object";
            m_compileParams << arg;
            foundCompileObject = true;
            continue;
        }
        
       // expect this is the filename to compile (we cannot compile more than one file atm!)
        if(arg.length() > 0 && !(arg.startsWith("/") || arg.startsWith("-"))) {
            m_sourceFileName = arg;
//            qDebug() << "m_sourceFileName:" << m_sourceFileName;
            continue;
        }
        
        // if we don't know, append to both
        m_preprocessParams << arg;
        m_compileParams << arg;
    }
    
    if(m_outputName.length() == 0) {
        m_outputName = QFileInfo(m_sourceFileName).baseName() + ".obj";
//        qDebug() << "output name is:" << m_outputName;
    }

    // in case the filename has known extension, enable compilation for a
    // specific language. There also exist the options /Tcfilename and /Tpfilename, 
    // which are not used by cmake at least ;-)
    if(m_sourceFileName.toLower().endsWith(".c")) {
//        qDebug() << "endswith .c:" << m_sourceFileName;
        if(!m_compileParams.contains("/TC")) m_compileParams.append("/TC");
    }

    if(m_sourceFileName.toLower().endsWith(".cpp")
    || m_sourceFileName.toLower().endsWith(".cc")
    || m_sourceFileName.toLower().endsWith(".cxx")) {
        if(!m_compileParams.contains("/TP")) m_compileParams.append("/TP");
    }

    qDebug() << "compile params:" << m_compileParams;
    if(foundCompileObject) {
//        qDebug() << "found /c, so we can compile this";
        return true;
    }

    if(m_sourceFileName.length() != 0) {
//        qDebug() << "found at least a source file, falling back now";
        m_fallback = true;
        return true;
    }
    return false;
}

