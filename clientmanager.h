#ifndef _clientmanager_h
#define _clientmanager_h

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtGui/QWidget>

class Client;
class Job;
class QTcpServer;
class Server;

/**
  * This class takes care of all Client objects on the scheduler site and essentially is the scheduler. 
  * It listens on the local computer for incoming connections from new Load objects and assigns them via
  * tcp a free core on one of the client machines. It keeps track of all Clients currently attached to
  * this scheduler.
  * FIXME: The parent widget is needed for correct output messages
  * FIXME: Maybe this can be changed to directly become a QAbstractItemModel
  */

class ClientManager : public QObject {
    Q_OBJECT
    public:
        ClientManager(Server *parent=0);
        
        bool enabled() const { return m_enabled; }
        void addClient(Client *cl);
        Client *client(int cln) { return m_clientList[cln]; }
        QList<QPointer<Client>> clientList() { return m_clientList; }
        int clientCount() const { return m_clientList.count(); }
        int coreCount() const;

    signals:
        void clientAdded(int cln);
        void clientInfoChanged(int cln);
        void clientLeft(int cln);
        
    public slots:
        void addNewClient();
        void updateClient();
        void removeClient();
        
        void assignJob(Job *job = 0);
        void processFinishedJob(Job *job);
        void setEnabled(bool enabled);
        
    private:
        QPointer<Server> m_server;
        QList<QPointer<Client>> m_clientList;
        QList<QPointer<Job>> m_todoJobs;
        QTcpServer *m_tcpServer;
        bool m_enabled;
};
#endif