qdcl is a distributed compiler similar to distcc and icecream for windows.
The source code is released under GPL v2 or later and owner of copyright is Patrick Spendrin.