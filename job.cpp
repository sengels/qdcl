#include "job.h"

#include <QtCore/QDebug>
#include <QtCore/QTimer>

#include "client.h"

quint64 Job::s_maxId = 0;

Job::Job()
    : m_id(++s_maxId)
    , m_requester(0)
    , m_status(uninitialized)
{
//    qDebug() << "generated Job #" << m_id;
}

void Job::setRequester(Client *cl)
{
    m_requester = cl;
}

Client *Job::requester() const
{
    return m_requester;
}

void Job::terminate()
{
    setStatus(Job::finished);
}

quint64 Job::id() const
{
    return m_id;
}

Job::jobStatus Job::status() const
{
    return m_status;
}

void Job::setStatus(Job::jobStatus stat)
{
    m_status = stat;
    emit statusChanged(m_status);
//    qDebug() << "setting status" << stat << "on job with id" << this->id();

    switch(m_status) {
        case Job::finished:
            {
                emit jobFinished(this);
                break;
            }
        case Job::waiting:
            {
                break;
            }
        case Job::running:
            {
                break;
            }
        default:
            qDebug() << "failed status set!";
    };
}

#include "job.moc"