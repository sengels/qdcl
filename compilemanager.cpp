#include "compilemanager.h"

#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkAddressEntry>
#include <QtNetwork/QTcpServer>

#include "compilejob.h"
#include "scheddialog.h"

CompileManager::CompileManager(Server* server)
 : m_tcpServer(0),
   m_server(server)
{
    m_tcpServer = new QTcpServer(this);

    connect(m_tcpServer, SIGNAL(newConnection()), this, SLOT(addNewJob()));

    if (!m_tcpServer->listen(QHostAddress(m_server->ownIp()), 34567)) {
        qDebug() << QString("Unable to start the server: %1.").arg(m_tcpServer->errorString());
        return;
    }
}

void CompileManager::addNewJob()
{
    CompileJob* job = new CompileJob(this, m_tcpServer->nextPendingConnection());
    connect(job, SIGNAL(finished()), job, SLOT(markFinishedJob()));
    connect(job, SIGNAL(fileReceived()), job, SLOT(start()));
    m_jobList.append(job);
//    qDebug() << "adding new job in compileManager!";
}

QString CompileManager::dataPath() const
{
    // FIXME: this must be taken from the settings
    return QString("C:/qdcl/tmp/");
}


#include "compilemanager.moc"