#include "clientmanager.h"

#include <QtCore/QString>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtGui/QMessageBox>

#include "client.h"
#include "localclient.h"
#include "job.h"
#include "scheddialog.h"

ClientManager::ClientManager(Server *p)
    : m_enabled(false),
      m_server(p)
{
    m_tcpServer = new QTcpServer(this);
}

int ClientManager::coreCount() const
{
    int coreSum = 0;
    foreach(Client*cl, m_clientList) {
        coreSum += cl->coreNumber();
    }
    return coreSum;
}

void ClientManager::addClient(Client *cl)
{
    m_clientList.append(cl);
    emit clientAdded(m_clientList.count() - 1);
}

void ClientManager::updateClient()
{
    Client *cl = static_cast<Client*>(sender());
    const int clientnum = m_clientList.indexOf(cl);
    
//    qDebug() << "client #1 changed:" << clientnum << cl << cl->name();
    
    if(clientnum < 0) return;
    
    emit clientInfoChanged(clientnum);
}

void ClientManager::addNewClient()
{
    Client *cl = new Client( this, m_tcpServer->nextPendingConnection() );
    connect(cl, SIGNAL(clientDisconnected()), this, SLOT(removeClient()));
    connect(cl, SIGNAL(informationChanged()), this, SLOT(updateClient()));
    connect(cl, SIGNAL(jobRequested()), this, SLOT(assignJob()));
    cl->requestInfo();

    addClient( cl );
}

void ClientManager::removeClient()
{
    Client *cl = static_cast<Client*>(sender());
    const int clientnum = m_clientList.indexOf(cl);
    
//    qDebug() << "client #1 disconnected:" << clientnum << cl;
    
    if(clientnum < 0) return;
    
    emit clientLeft(clientnum);
    delete m_clientList[clientnum];
    m_clientList.removeAt(clientnum);
}

void ClientManager::processFinishedJob(Job *job)
{
    
    if(!m_todoJobs.isEmpty()) {
        qDebug() << "assigning job with id" << m_todoJobs.first()->id() << job->id() << m_todoJobs.length();
        assignJob(m_todoJobs.first());
        m_todoJobs.removeFirst();
    }
}

void ClientManager::assignJob(Job *job)
{
    Client *requestingClient = static_cast<Client*>(sender());

    if(job == 0) job = new Job;
    qDebug() << "a brand new job" << job;

    if(coreCount() <= 0) {
        qDebug() << "got an assign job for id" << job->id() << "but coreCount is still 0";
        job->setStatus(Job::waiting);
        requestingClient->answerRequestJob(job, QString("0.0.0.0"), 0);
        m_todoJobs.append(job);
        return;
    }
    
    //qDebug() << "got an assign job for id" << job->id();

    int coreNumber = qrand() % coreCount();
    int assignedCore = 0;
    int assignedClientNum = (coreNumber == 0) ? 0 : -1;
    while(assignedCore < coreNumber) {
        assignedCore += m_clientList[++assignedClientNum]->coreNumber();
    }
    Client *assignedClient = m_clientList[assignedClientNum];
    
    connect(job, SIGNAL(jobFinished(Job*)), assignedClient, SLOT(removeJob(Job*)));

    // first try to disconnect from slot, so that we don't call processFinishedJob multiple times
    disconnect(assignedClient, SIGNAL(jobFinished(Job*)), this, SLOT(processFinishedJob(Job*)));
    connect(assignedClient, SIGNAL(jobFinished(Job*)), this, SLOT(processFinishedJob(Job*)));
    assignedClient->addJob(job);
    qDebug() << "answer requested job:" << assignedClientNum << assignedClient->address() << assignedClient->port();

    job->setStatus(Job::running);
    requestingClient->answerRequestJob(job, assignedClient->address(), assignedClient->port());
}

void ClientManager::setEnabled(bool enabled)
{
    m_enabled = enabled;
    if(m_enabled) {
        if (!m_tcpServer->listen(QHostAddress(m_server->ownIp()), 12345)) {
            QMessageBox::critical(m_server, tr("qdclsched"),
                                  tr("Unable to start the server: %1.")
                                  .arg(m_tcpServer->errorString()));
            return;
        }

        connect(m_tcpServer, SIGNAL(newConnection()), this, SLOT(addNewClient()));
/*        Client *client = qobject_cast<Client*>(new LocalClient(m_server));
        m_clientList.append(client);
        emit clientAdded(m_clientList.count() - 1);*/
    } else {
        disconnect(m_tcpServer, SIGNAL(newConnection()), this, SLOT(addNewClient()));
        if(m_tcpServer->isListening()) m_tcpServer->close();
        qDeleteAll(m_clientList);
    }
}

#include "clientmanager.moc"