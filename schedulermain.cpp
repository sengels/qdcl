#include <QtGui/QApplication>

#include "scheddialog.h"
#include "schedulerlocator.h"

int main(int argc, char**argv)
{
    QApplication app(argc, argv);
    Server server;
    server.switchWidgets(Scheduler);
    server.show();
    return app.exec();
}