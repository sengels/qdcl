#ifndef _common_h
#define _common_h

namespace Magix {
    typedef enum {
        RequestJob = 0x2101,
        AnswerRequestJob,
        FinishJob,
        SendSlaveInfo,
        RequestSlaveInfo,
        SignalJobFinished,
        SignalJobFailed,
        SignalIgnoreData,
        RequestCompileJob,
        MarkCompileJobFinished
    } magix;
};
#endif