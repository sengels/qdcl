#include "visualizer.h"

#include <QtCore/QDebug>
#include <QtGui/QLabel>
#include <QtGui/QTableWidget>
#include <QtGui/QTableWidgetItem>
#include <QtGui/QVBoxLayout>

#include "clientmanager.h"
#include "client.h"

Visualizer::Visualizer(ClientManager *man, QWidget *parent)
    : QWidget(parent),
      m_manager(man),
      m_label(0),
      m_tableWidget(0)
{
    m_label = new QLabel;
    m_label->setText("Overview");
    m_tableWidget = new QTableWidget(0, 4);
    
    m_tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("name"));
    m_tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("address"));
    m_tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("port"));
    m_tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("cores"));
    
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(m_label);
    layout->addWidget(m_tableWidget);
    
    setLayout(layout);
    
    connect(m_manager, SIGNAL(clientAdded(int)), this, SLOT(addEntry(int)));
    connect(m_manager, SIGNAL(clientInfoChanged(int)), this, SLOT(updateEntry(int)));
    connect(m_manager, SIGNAL(clientLeft(int)), this, SLOT(removeEntry(int)));
}

void Visualizer::addEntry(int line)
{
    QTableWidgetItem *item_name = new QTableWidgetItem(m_manager->client(line)->name());
    QTableWidgetItem *item_address = new QTableWidgetItem(m_manager->client(line)->address());
    QTableWidgetItem *item_port = new QTableWidgetItem(m_manager->client(line)->port());
    QTableWidgetItem *item_cores = new QTableWidgetItem(QString("%1").arg(m_manager->client(line)->coreNumber()));
    
    item_name->setFlags(Qt::ItemIsEnabled);
    item_address->setFlags(Qt::ItemIsEnabled);
    item_port->setFlags(Qt::ItemIsEnabled);
    item_cores->setFlags(Qt::ItemIsEnabled);

    const int rowCount = m_tableWidget->rowCount();
    m_tableWidget->insertRow(rowCount);

    m_tableWidget->setItem(rowCount, 0, item_name);
    m_tableWidget->setItem(rowCount, 1, item_address);
    m_tableWidget->setItem(rowCount, 2, item_port);
    m_tableWidget->setItem(rowCount, 3, item_cores);
}

void Visualizer::updateEntry(int line)
{
    m_tableWidget->item(line, 0)->setText(m_manager->client(line)->name());
    m_tableWidget->item(line, 1)->setText(m_manager->client(line)->address());
    m_tableWidget->item(line, 2)->setText(QString("%1").arg(m_manager->client(line)->port()));
    m_tableWidget->item(line, 3)->setText(QString("%1").arg(m_manager->client(line)->coreNumber()));
}

void Visualizer::removeEntry(int line)
{
    m_tableWidget->removeRow(line);
}

QSize Visualizer::minimumSizeHint() const
{
    return m_tableWidget->minimumSizeHint();
}

#include "visualizer.moc"