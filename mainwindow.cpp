#include "mainwindow.h"

#include <QtCore/QDebug>
#include <QtGui/QSystemTrayIcon>
#include <QtGui/QVBoxLayout>

#include "propertiesdialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , m_trayIcon(0)
    , m_propDialog(0)
{
    QVBoxLayout* mainLayout = new QVBoxLayout;
    m_propDialog = new PropertiesDialog;
    m_trayIcon = new QSystemTrayIcon(QIcon("C:/kde/download/svn-src/qdcl/emblem-favorite-32.png"), this);
    m_trayIcon->show();
    mainLayout->addWidget(m_propDialog);
    setLayout(mainLayout);
    qDebug() << "MainWindow:" << sizeHint();
}

/*QMessageBox msgBox;
msgBox.setText("The document has been modified.");
msgBox.exec();*/

MainWindow::~MainWindow()
{
    delete m_trayIcon;
}

#include "mainwindow.moc"