#include "client.h"

#include <QtCore/QDebug>
#include <QtCore/QDataStream>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include "common.h"
#include "job.h"
#include "clientmanager.h"

Client::Client(ClientManager *man, QTcpSocket *conn)
    : m_conn(conn),
      m_manager(man),
      m_coreNumber(0)
{
    connect(m_conn, SIGNAL(disconnected()), this, SIGNAL(clientDisconnected()));
    connect(m_conn, SIGNAL(readyRead()), this, SLOT(read()));
    connect(this, SIGNAL(jobFinished(Job*)), this, SLOT(removeJob(Job*)));
}

// the protected constructor for use by LocalClient
Client::Client(int localport, int coreNum)
    : m_conn(0)
    , m_port(localport)
    , m_coreNumber(coreNum)
{
}

void Client::requestInfo()
{
//    qDebug() << "requesting informations from client!";
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::RequestSlaveInfo;
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));

    m_conn->write(block);
}

QString Client::address() const
{
    return m_conn->peerAddress().toString();
}

quint16 Client::port() const
{
    return m_port;
}

QString Client::name() const
{
    return m_hostName;
}

int Client::coreNumber() const
{
    return m_coreNumber;
}

bool Client::containsId(quint64 id) const
{
    foreach(Job *j, m_jobs) {
        if(j->id() == id) {
            return true;
        }
    }
    return false;
}
Job* Client::getJobForId(quint64 id)
{
    foreach(Job *j, m_jobs) {
        if(j->id() == id) {
            return j;
        }
    }
    return NULL;
}

void Client::read()
{
    quint16 magic;
    QDataStream in(m_conn);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_conn->bytesAvailable() < (qint64)sizeof(qint64)) {
        qDebug() << "wait for blockSize";
        return;
    }

    in >> m_blockSize;

    if (m_conn->bytesAvailable() < m_blockSize) {
        qDebug() << "wait for full block of size" << m_blockSize;
        return;
    }
    
    in >> magic;
//    qDebug() << "received magic:" << magic;

    switch(magic) {
        case Magix::SignalJobFinished:
            ++m_coreNumber;
            break;

        case Magix::RequestJob:
            qDebug() << "a job request was received!";
            emit jobRequested();
            break;
        case Magix::FinishJob:
            {
                quint64 id;
                in >> id;
                foreach(Client *cl, m_manager->clientList()) {
                    if(cl->containsId(id)) {
                        cl->removeJob(cl->getJobForId(id));
                        m_blockSize = 0;
                        return;
                    }
                }
                qDebug() << "couldn't find job with id" << id << "in any currently connected client!";
                break;
            }
        case Magix::SendSlaveInfo:
            in >> m_coreNumber;
            in >> m_hostName;
            in >> m_port;
            
//            qDebug() << "Number of cores:" << m_coreNumber;
//            qDebug() << "remote Hostname:" << m_hostName;
//            qDebug() << "remote port:" << m_port;
            
            emit informationChanged();
            break;
            
        case Magix::SignalIgnoreData:
        default:
            qDebug() << "skipping data!";
            in.skipRawData(m_blockSize - sizeof(quint16));
            break;
    };
    m_blockSize = 0;
}

/**
  * assignJob is called on the assigned client
  */
void Client::addJob(Job *job)
{
    --m_coreNumber;
    emit informationChanged();
    m_jobs.append(job);
}

/**
  * removeJob is called on the assigned client
  */
void Client::removeJob(Job *job)
{
    const int i = m_jobs.indexOf(job);
    if(i >= 0) {
        m_jobs.removeAt(i);
        ++m_coreNumber;
        emit informationChanged();
    }
}

/**
  * answerRequestJob is called on the requesting client and send to the assignedClient
  */
void Client::answerRequestJob(Job *job, const QString& address, int port)
{
    qDebug() << "answerRequestJob" << job << address << port << m_conn->peerAddress() << m_conn->peerPort();
    QByteArray block;

    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::AnswerRequestJob;

    out << address;
    out << port;
    out << job->status();
    out << job->id();

    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));

    m_conn->write(block);
}

#include "client.moc"
