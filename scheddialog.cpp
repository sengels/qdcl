#include "scheddialog.h"

#include <QtCore/QList>
#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QCoreApplication>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtGui/QMessageBox>
#include <QtGui/QComboBox>
#include <QtGui/QVBoxLayout>

#include "visualizer.h"
#include "connectwidget.h"
#include "clientmanager.h"
#include "schedulerlocator.h"
#include "compilemanager.h"
#include "loadmanager.h"
#include "client.h"

Server::Server(QWidget *parent)
     : QDialog(parent),
       m_comboBox(0),
       m_visualizer(0),
       m_connectWidget(0),
       m_serverMode(Slave),
       m_layout(0),
       m_clientManager(0),
       m_compileManager(0),
       m_locator(0),
       m_port(12345)
{
    m_ip = ownIp();

    m_comboBox = new QComboBox;
    m_comboBox->insertItem(Slave, "slave");
    m_comboBox->insertItem(Scheduler, "scheduler");
    
    connect(m_comboBox, SIGNAL(activated(int)), this, SIGNAL(modeChanged(int)));
    connect(m_comboBox, SIGNAL(activated(int)), this, SLOT(switchWidgets(int)));

    m_connectWidget = new ConnectWidget(this);
    m_clientManager = new ClientManager(this);
    m_visualizer = new Visualizer(m_clientManager);
    m_locator = new SchedulerLocator(this);
    m_compileManager = new CompileManager(this);
    m_loadManager = new LoadManager(this);

    // FIXME: this is autoconnect, make this optional later on
    connect(m_locator, SIGNAL(schedulerFound(QString, quint16)), this, SLOT(setHost(QString, quint16)));
    connect(m_locator, SIGNAL(schedulerFound(QString, quint16)), this, SLOT(connectServer()));
    
    connect(m_loadManager, SIGNAL(connectStatusChanged(bool)), m_connectWidget, SLOT(changeConnectStatus(bool)));

    m_layout = new QVBoxLayout;
    m_layout->addWidget(m_comboBox);
    m_layout->addWidget(m_visualizer);
    m_layout->addWidget(m_connectWidget);

    m_visualizer->setVisible(false);

    m_locator->requestScheduler();

    setLayout(m_layout);
    
    setWindowTitle(tr("qdclsched"));
}

QString Server::ownIp() const
{
    QString ip;
    foreach(QNetworkInterface nic, QNetworkInterface::allInterfaces()) {
        if(nic.flags() & QNetworkInterface::IsUp && nic.flags() & QNetworkInterface::IsRunning) {
            foreach(QNetworkAddressEntry entry, nic.addressEntries()) {
                const QHostAddress address = entry.ip();
                if(address != QHostAddress::LocalHost && address.toIPv4Address() && !address.toString().startsWith("169.254.")) {
                    ip = address.toString();
                    break;
                }
            }
        }
    }

    if (ip.isEmpty())
        ip = "169.254.0.0"; // use a broken ip in case of problems
    return ip;
}

void Server::setHost(const QString& ip, quint16 port)
{
    m_ip = ip;
    m_port = port;
    m_connectWidget->setHost(m_ip, m_port);
}

void Server::setIp(const QString& ip)
{
    m_ip = ip;
    emit ipChanged(m_ip);
    m_connectWidget->setHost(m_ip, m_port);
}

void Server::setPort(quint16 port)
{
    m_port = port;
    emit portChanged(port);
    m_connectWidget->setHost(m_ip, m_port);
}

void Server::setPortString(const QString& port)
{
    setPort(port.toUInt());
}

QString Server::ip()
{
    return m_ip;
}

quint16 Server::port()
{
    return m_port;
}

void Server::switchWidgets(int mode)
{
    if(mode == m_serverMode) return;

    m_serverMode = (ServerMode)mode;
    m_locator->switchSchedulerMode();
    if(mode == Scheduler) {
        m_connectWidget->setVisible(false);
        m_visualizer->setVisible(true);
        m_clientManager->setEnabled(true);
        m_ip = ownIp();
        m_locator->announceScheduler();
        connectServer();
    } else {
        disconnectServer();
        m_visualizer->setVisible(false);
        m_connectWidget->setVisible(true);
        m_clientManager->setEnabled(false);
    }
    resize(m_layout->sizeHint());
}

void Server::connectServer()
{
    m_loadManager->connectDefault();
}

void Server::disconnectServer()
{
    m_loadManager->disconnectServer();
}

#include "scheddialog.moc"