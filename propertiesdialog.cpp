#include "propertiesdialog.h"

#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QVariant>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QVBoxLayout>
#include <QtGui/QIcon>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>

PropertiesDialog::PropertiesDialog(QWidget *parent)
    : QWidget(parent)
    , m_settings("qdcl", "qdcl")
    , m_buttons(0)
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    QGridLayout *gridLayout = new QGridLayout;
    foreach(const QString& key, m_settings.allKeys()) {
        QLineEdit* lineEdit = new QLineEdit;
        QLabel* label = new QLabel(key);
        m_lineEdits << lineEdit;
        m_labels << label;
        m_keys << key;
        label->setBuddy(lineEdit);
        lineEdit->setText(m_settings.value(key).toString());
        const int rowCount = gridLayout->rowCount();
        gridLayout->addWidget(label, rowCount, 0);
        gridLayout->addWidget(lineEdit, rowCount, 1);
    }
    mainLayout->addLayout(gridLayout);
    m_buttons = new QDialogButtonBox(QDialogButtonBox::Cancel|QDialogButtonBox::Ok, Qt::Horizontal, this);
    connect(m_buttons, SIGNAL(accepted()), this, SLOT(applySettings()));
    connect(m_buttons, SIGNAL(rejected()), this, SLOT(resetSettings()));
    mainLayout->addWidget(m_buttons);
    setLayout(mainLayout);
    qDebug() << "PropertiesDialog:" << sizeHint();
}

PropertiesDialog::~PropertiesDialog()
{
    qDeleteAll(m_lineEdits);
    qDeleteAll(m_labels);
    delete m_buttons;
}

void PropertiesDialog::applySettings()
{
    int i = 0;
    foreach(const QString& key, m_settings.allKeys()) {
        m_settings.setValue(key, m_lineEdits[i++]->text());
    }
}

void PropertiesDialog::resetSettings()
{
    int i = 0;
    foreach(const QString& key, m_settings.allKeys()) {
        m_lineEdits[i++]->setText(m_settings.value(key).toString());
    }
}

#include "propertiesdialog.moc"