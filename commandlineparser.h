#ifndef _commandlineparser_h
#define _commandlineparser_h

#include <QtCore/QStringList>

class CommandlineParser {
    public:
        CommandlineParser(QStringList parse);
        bool parse();
        QStringList preprocessResult() const;
        QStringList fallbackResult() const;
        QStringList compileResult() const;
        
        QString sourceFile() const;
        QString objectFile() const;
        QString debugFile() const;
        bool fallback() const;

    private:
        bool parseMSVCParameters(QStringList param);
        static QStringList resolveAtSyntax(const QStringList& param);

        QStringList     m_parameters;
        bool            m_fallback;
        QStringList     m_compileParams;
        QStringList     m_fallbackParams;
        QStringList     m_preprocessParams;
        QString         m_sourceFileName;
        QString         m_outputName;
        QString         m_debugName;
};
#endif /* _commandlineparser_h */
