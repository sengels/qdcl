#include "clientobject.h"

#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QHostAddress>
#include <QtCore/QDataStream>
#include <QtCore/QTimer>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QCoreApplication>

_Client::_Client()
 : m_broadcast(QHostAddress::Broadcast)
{
//    m_tcpSocket = new QTcpSocket(this);
    m_udpSocket = new QUdpSocket(this);
    QByteArray datagram = "Broadcast message " + QByteArray::number(1);
    m_broadcast = QHostAddress("192.168.2.255");
    m_udpSocket->writeDatagram(datagram.data(), datagram.size(), m_broadcast, 45454);

    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(readFortune()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(m_tcpSocket, SIGNAL(disconnected()),
            qApp, SLOT(quit()));
    qDebug() << "arguments:" << qApp->arguments();
}

void _Client::requestNewFortune()
{
    m_blockSize = 0;
//    m_tcpSocket->abort();
    // lets try to connect to a general broadcast address
//    m_tcpSocket->connectToHost("255.168.2.255", 12345);
}

void _Client::readFortune()
{
    QDataStream in(m_tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_blockSize == 0) {
        if (m_tcpSocket->bytesAvailable() < (int)sizeof(quint16))
            return;

        in >> m_blockSize;
    }

    if (m_tcpSocket->bytesAvailable() < m_blockSize)
        return;

    QByteArray nextFortune;
    in >> nextFortune;

    if (nextFortune == m_currentFortune) {
        qDebug() << "request new fortune!";
        QTimer::singleShot(0, this, SLOT(requestNewFortune()));
        return;
    }

    m_currentFortune = nextFortune;
    qDebug() << "got fortune:" << m_currentFortune;
}

void _Client::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        qDebug() << "remote host closed connection!";
        m_tcpSocket->abort();
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << "The host was not found. Please check the host name and port settings.";
        m_tcpSocket->abort();
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << "The connection was refused by the peer. "
                    "Make sure the fortune server is running, "
                    "and check that the host name and port "
                    "settings are correct.";
        m_tcpSocket->abort();
        break;
    default:
        qDebug() << QString("The following error occurred: %1.").arg(m_tcpSocket->errorString());
        m_tcpSocket->abort();
    }
}

#include "clientobject.moc"
