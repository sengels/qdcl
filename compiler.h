#ifndef _compiler_h
#define _compiler_h

#include <QtCore/QByteArray>
#include <QtCore/QFile>
#include <QtCore/QPointer>
#include <QtCore/QSettings>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>

class CommandlineParser;

/**
  * This class parses the command, preprocesses the file and provides the fallback compiler.
  * It also transmits the request & data to the local qdclsched.exe application and waits for
  * the return information.
  */
class Compiler {
    public:
        Compiler();
        int compile();
        bool parseParameters(QStringList params);
    private:
        int fallbackCompile();
        bool preprocess();

        QString& logFileName();
        inline QString compilerPath() const;

    private:
        CommandlineParser *m_parser;
        QSettings       m_settings;

        QByteArray      m_content;
        QString         m_logFileName;
        QFile           m_logFile;
        QTextStream     m_logStream;
};
#endif