#ifndef _job_h
#define _job_h

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QString>

class Client;

/**
  * A Job is exactly one compile task, which is needed to keep track of 
  * assignments to each executor
  */
class Job : public QObject {
    Q_OBJECT
    public:
        Job();

        typedef enum enumJobStatus {
            errorOccured = -1,
            finished = 0,
            uninitialized,
            waiting,
            running
        } jobStatus;
        /**
          * returns the status of this job
          */
        jobStatus status() const;
        quint64 id() const;

        void setRequester(Client *cl);
        Client *requester() const;

    public slots:
        void setStatus(jobStatus stat);
        
    private slots:
        void terminate();

    signals:
        void statusChanged(jobStatus s);
        void jobFinished(Job *job);

    private:
        QPointer<Client> m_requester;
        jobStatus m_status;

        int m_id;
        static quint64 s_maxId;
};
#endif