#include "connectwidget.h"

#include <QtCore/QDebug>
#include <QtGui/QIntValidator>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QGridLayout>
#include <QtGui/QPushButton>

#include "scheddialog.h"


ConnectWidget::ConnectWidget(Server *parent)
    : QWidget(parent)
    , m_server(parent)
{
    m_hostLabel = new QLabel(tr("&Server name:"));
    m_portLabel = new QLabel(tr("S&erver port:"));

    m_hostLineEdit = new QLineEdit(m_server->ip());
    m_portLineEdit = new QLineEdit(QString("%1").arg(m_server->port()));
    m_portLineEdit->setValidator(new QIntValidator(1, 65535, this));

    m_hostLabel->setBuddy(m_hostLineEdit);
    m_portLabel->setBuddy(m_portLineEdit);
    
    m_connectButton = new QPushButton(tr("connect"));
    m_disconnectButton = new QPushButton(tr("disconnect"));
    m_disconnectButton->setEnabled(false);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(m_hostLabel, 0, 0);
    mainLayout->addWidget(m_hostLineEdit, 0, 1);
    mainLayout->addWidget(m_portLabel, 1, 0);
    mainLayout->addWidget(m_portLineEdit, 1, 1);
    mainLayout->addWidget(m_connectButton, 2, 0);
    mainLayout->addWidget(m_disconnectButton, 2, 1);
    setLayout(mainLayout);

    setWindowTitle(tr("qdclsched"));
    
    connect(m_hostLineEdit, SIGNAL(textChanged(QString)), m_server, SLOT(setIp(QString)));
    connect(m_portLineEdit, SIGNAL(textChanged(QString)), m_server, SLOT(setPortString(QString)));

    connect(m_connectButton, SIGNAL(clicked()), m_server, SLOT(connectServer()));
    connect(m_disconnectButton, SIGNAL(clicked()), m_server, SLOT(disconnectServer()));
    m_portLineEdit->setFocus();
}

void ConnectWidget::changeConnectStatus(bool st)
{
    m_connectButton->setEnabled(!st);
    m_disconnectButton->setEnabled(st);
    m_hostLabel->setEnabled(!st);
    m_portLabel->setEnabled(!st);
    m_hostLineEdit->setEnabled(!st);
    m_portLineEdit->setEnabled(!st);
}

void ConnectWidget::setHost(const QString& ip, quint16 port)
{
    m_hostLineEdit->setText(ip);
    m_portLineEdit->setText(QString("%1").arg(port));
}

#include "connectwidget.moc"