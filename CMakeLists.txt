project(qdcl)

enable_testing()
cmake_minimum_required(VERSION 2.6)

find_package(Qt4 REQUIRED)

include_directories(${QT_INCLUDES} ${CMAKE_BINARY_DIR})
add_definitions(${QT_DEFINITIONS})

if(BUILD_STATIC)
    foreach(flag_var
            CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
            CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO)
       if(${flag_var} MATCHES "/MD")
          string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}")
       endif(${flag_var} MATCHES "/MD")
    endforeach(flag_var)

    set(additional_libs Imm32 Winmm ws2_32)
endif(BUILD_STATIC)


# we want that cl.exe is not in the path, nor in the current directory!
set (EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)
#set (LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)

##############################################################################
# the actual compiler
##############################################################################

set(qdcl_SRCS
    compilerequest.cpp
    commandlineparser.cpp
    compiler.cpp
    main.cpp
)

qt4_automoc(${qdcl_SRCS})

add_executable(qdcl ${qdcl_SRCS})
set_target_properties(qdcl PROPERTIES OUTPUT_NAME cl)
target_link_libraries(qdcl ${QT_QTCORE_LIBRARY} ${QT_QTNETWORK_LIBRARY} ${additional_libs})

##############################################################################
# the scheduler
##############################################################################

set(qdclsched_SRCS
    clientmanager.cpp
    client.cpp
    compilejob.cpp
    compilemanager.cpp
    connectwidget.cpp
    job.cpp
    load.cpp
    loadmanager.cpp
    localclient.cpp
    scheddialog.cpp
    schedulerlocator.cpp
    schedulermain.cpp
    visualizer.cpp
)

qt4_automoc(${qdclsched_SRCS})

add_executable(qdclsched WIN32 ${qdclsched_SRCS})
target_link_libraries(qdclsched ${QT_QTMAIN_LIBRARY} ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY} ${QT_QTNETWORK_LIBRARY} ${additional_libs})


##############################################################################
# the client
##############################################################################

set(qdclclient_SRCS
    mainwindow.cpp
    propertiesdialog.cpp
    clientmain.cpp
)

qt4_automoc(${qdclclient_SRCS})

add_executable(qdclclient WIN32 ${qdclclient_SRCS})
target_link_libraries(qdclclient ${QT_QTMAIN_LIBRARY} ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY} ${additional_libs})


##############################################################################
# the tests
##############################################################################
add_subdirectory(tests)

install(TARGETS qdclsched qdcl qdclclient RUNTIME DESTINATION bin
                                          LIBRARY DESTINATION lib
                                          ARCHIVE DESTINATION lib)
