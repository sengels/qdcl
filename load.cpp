#include "load.h"

#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QSharedMemory>
#include <QtCore/QTimer>
#include <QtNetwork/QLocalSocket>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include "common.h"

Load::Load()
 : m_localSocket(0)
 , m_tcpSocket(0)
 , m_blockSize(0)
{
    m_sharedMemory = new QSharedMemory;
}

void Load::setLocalSocket(QLocalSocket *socket)
{
    m_localSocket = socket;
    connect(m_localSocket, SIGNAL(readyRead()), this, SLOT(readLocalData()));
    connect(m_localSocket, SIGNAL(disconnected()), this, SIGNAL(terminated()));
    if(m_localSocket->bytesAvailable() > 0) readLocalData();
}

void Load::setRemoteInfo(const QString& address, quint16 port, quint64 id)
{
    m_address = address;
    m_port = port;
    m_id = id;
}

void Load::start()
{
    m_tcpSocket = new QTcpSocket;
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(readTcpData()));
    m_tcpSocket->connectToHost(m_address, m_port);
    const bool waitForConnected = 
    m_tcpSocket->waitForConnected();
    qDebug() << "connect to" << m_address << ":" << m_port << "is connected:" << waitForConnected << m_tcpSocket->isValid();
    m_sharedMemory->setKey(m_key);
    m_sharedMemory->attach();
    m_sharedMemory->lock();
    
    QBuffer buffer;
    QDataStream in(&buffer);

    buffer.setData((char*)m_sharedMemory->constData(), m_sharedMemory->size());
    buffer.open(QBuffer::ReadOnly);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << m_key;
    out << m_params;
    out << buffer.read(m_size);
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));
    const int dataSize = m_tcpSocket->write(block);
    qDebug() << this << "write data of size:" << dataSize << "to peer:" << m_tcpSocket->peerAddress() << "key:" << m_key << "size:" << m_size;
    buffer.close();
    m_sharedMemory->unlock();
    m_sharedMemory->detach();
    qDebug() << "job assigned to" << m_address << ":" << m_port << "and job id:" << m_id << m_key << m_tcpSocket->peerAddress();
}

void Load::readLocalData()
{
    qDebug() << "now reading new local request!" << this;
    QDataStream in(m_localSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_localSocket->bytesAvailable() < (qint64)sizeof(qint64)) {
        qDebug() << "not enough bytes available for blockSize";
        return;
    }

//    dumpCurrentStream();

    if(m_blockSize == 0) in >> m_blockSize;

    if (m_localSocket->bytesAvailable() < m_blockSize) {
        qDebug() << "not enough bytes available for complete block:" << m_blockSize << m_localSocket->bytesAvailable();
        return;
    }
//    dumpCurrentStream();

    quint16 magic;

    in >> magic;

//    qDebug() << "received magic:" << magic;
    
    switch(magic) {
        case Magix::RequestCompileJob:
            {
                in >> m_size;
                in >> m_key;
                in >> m_params;
                qDebug() << this << "requestCompileJob for data: " << m_size << m_key << m_params << "!";
                emit jobRequested();
                m_blockSize = 0;
                break;
            }
        default:
            {
                qDebug() << "couldn't find this magic:" << magic;
            }
    }
//    qDebug() << "finished!";
    m_blockSize = 0;
}

void Load::readTcpData()
{
//    qDebug() << "now reading new tcp request!";
    QDataStream in(m_tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_tcpSocket->bytesAvailable() < (qint64)sizeof(qint64)) {
        qDebug() << "tcp not enough bytes available for blockSize";
        return;
    }

//    dumpCurrentStream();

//    qDebug() << "tcp blockSize:" << m_blockSize;
    if(m_blockSize == 0) in >> m_blockSize;

    if (m_tcpSocket->bytesAvailable() < m_blockSize) {
        qDebug() << "tcp not enough bytes available for complete block:" << m_blockSize << m_localSocket->bytesAvailable();
        return;
    }
//    dumpCurrentStream();

    quint16 magic;

    in >> magic;

//    qDebug() << "tcp received magic:" << magic;
    
    switch(magic) {
        case Magix::MarkCompileJobFinished:
            {
                in >> m_exitCode;
                in >> m_errorOutput;
                in >> m_content;
                qDebug() << "tcp MarkCompileJobFinished with exitCode" << m_exitCode << "errorOutputSize:" << m_errorOutput.size() << "contentSize:" << m_content.size() << "!";
                finishLocalJob();
                emit terminated();
                m_blockSize = 0;
                break;
            }
        default:
            {
                qDebug() << "couldn't find this magic:" << magic;
            }
    }
//    qDebug() << "tcp finished!";
    m_blockSize = 0;
}

void Load::finishLocalJob()
{
//    qDebug() << "localFinish!" << this;

    delete m_sharedMemory;
    m_sharedMemory = new QSharedMemory;
    m_sharedMemory->setKey(m_key + "-return");
    while(m_sharedMemory->attach()) {
        QString key = m_sharedMemory->key() + "X";
        m_sharedMemory->detach();
        m_sharedMemory->setKey(key);
    }
    if(!m_sharedMemory->create(m_errorOutput.size() + m_content.size())) {
        qDebug() << "failed to create sharedmemory of size" << (m_errorOutput.size() + m_content.size()) << m_sharedMemory->errorString() << m_sharedMemory->key();
    }

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::FinishJob;
    out << m_sharedMemory->key();
    out << (quint16)m_exitCode;
    out << (quint32)m_errorOutput.size();
    out << (quint32)m_content.size();
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));

    m_sharedMemory->lock();

    int memsize = m_sharedMemory->size();
    char *dest = (char*)m_sharedMemory->data();
    const char *errorOutput = m_errorOutput.data();
    memcpy(dest, errorOutput, qMin(memsize, m_errorOutput.size()));

    memsize -= m_errorOutput.size();
    dest = (char*)m_sharedMemory->data() + m_errorOutput.size();
    const char *content = m_content.data();
    memcpy(dest, content, qMin(memsize, m_content.size()));

    m_sharedMemory->unlock();

    m_localSocket->write(block);
    m_localSocket->close();
}


quint64 Load::id() const
{
    return m_id;
}
#include "load.moc"