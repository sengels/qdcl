#include "localclient.h"

#include <QtCore/QThread>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QHostInfo>

LocalClient::LocalClient(Server* server)
 : Client(34567, QThread::idealThreadCount())
 , m_server(server)
{
}

QString LocalClient::name() const
{
    return QHostInfo::localHostName();
}

QString LocalClient::address() const
{
    return m_server->ownIp();
}

#include "localclient.moc"