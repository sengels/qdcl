#include "compilejob.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QProcess>
#include <QtCore/QTemporaryFile>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include "common.h"
#include "compilemanager.h"

CompileJob::CompileJob(CompileManager* man, QTcpSocket* socket)
 : m_socket(socket)
 , m_manager(man)
 , m_blockSize(0)
{
    qDebug() << this << "new compileJob started!";
    connect(m_socket, SIGNAL(readyRead()), this, SLOT(readData()));
    // lock the mutex so that the compile job will wait 
    m_mutex.lock();
    qDebug() << this << "locked";
}

void CompileJob::readData()
{
    QDataStream in(m_socket);
    in.setVersion(QDataStream::Qt_4_0);

    if (m_socket->bytesAvailable() < (qint64)sizeof(qint64)) {
        qDebug() << this << "compile: wait for blockSize";
        return;
    }

    if(m_blockSize == 0) in >> m_blockSize;

    if (m_socket->bytesAvailable() < m_blockSize) {
        qDebug() << this << "compile: wait for full block of size" << m_blockSize << "current:" << m_socket->bytesAvailable();
        return;
    }
    
    in >> m_key;
    in >> m_params;
    in >> m_content;
    
    qDebug() << "starting compile job with key:" << m_key << "params:" << m_params << "content size:" << m_content.size();
    m_tempFile = new QTemporaryFile;
    m_tempFile->setFileTemplate(m_manager->dataPath() + "src/src-XXXXXX.i");
    m_tempFile->setAutoRemove (false);
    m_tempFile->open();
    m_tempFile->write(qUncompress(m_content));
    m_tempFileName = m_tempFile->fileName();
//    qDebug() << "fileName:" << m_tempFileName;
    m_tempFile->close();
    delete m_tempFile;
    // unlock the mutex when the file is completely filled
    m_mutex.unlock();
    m_blockSize = 0;

    emit fileReceived();
}

void CompileJob::run()
{
    m_process = new QProcess;
//    qDebug() << "trying to execute some commands";
    m_mutex.lock();
    QByteArray vcinstalldir = qgetenv("VCINSTALLDIR");
    QString cmd = "\"" + QString(vcinstalldir) + "\\bin\\cl.exe\" " + m_params 
                            + " /Fo" + QDir::toNativeSeparators(m_manager->dataPath() + "obj/" + m_key + ".obj") 
                            + " " + QDir::toNativeSeparators(m_tempFileName);
//    qDebug() << "executing command:" << cmd;
    m_process->start(cmd);

    if (!m_process->waitForFinished()) {
        qDebug() << "error: waitForFinished failed!";
        return;    
    }
    m_mutex.unlock();

    // this might have to be send directly to the other computer
    m_errorOutput = m_process->readAllStandardError();
    m_errorOutput = qCompress(m_errorOutput);
    m_standardOutput = m_process->readAllStandardOutput();
    m_standardOutput = qCompress(m_standardOutput);
    QFile objfile(m_manager->dataPath() + "obj/" + m_key + ".obj");
    objfile.open(QIODevice::ReadOnly);
    m_content = objfile.readAll();
    m_content = qCompress(m_content);
    objfile.close();
//    qDebug() << "compressed object file:" << m_content.size();
}

void CompileJob::markFinishedJob()
{
    qDebug() << "markFinishedJob now underway!" << m_socket->peerAddress();
//    qDebug() << "sending data:" << m_errorOutput.size() << qUncompress(m_errorOutput) << m_errorOutput;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << (qint64)0;
    out << (quint16)Magix::MarkCompileJobFinished;
    out << (quint16)m_process->exitCode();
    out << m_errorOutput;
    out << m_content;
    out.device()->seek(0);
    out << (qint64)(block.size() - sizeof(qint64));
    
    m_socket->write(block);
}


#include "compilejob.moc"