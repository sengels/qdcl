#ifndef _connectwidget_h
#define _connectwidget_h

#include <QtCore/QPointer>
#include <QtGui/QWidget>

class QLabel;
class QLineEdit;
class QPushButton;
class Server;

class LoadManager;

/**
  * This Widget is used to enter the details for the server to be used
  */
class ConnectWidget : public QWidget {
    Q_OBJECT
    public:
        ConnectWidget(Server *parent = 0);

        void setHost(const QString& ip, quint16 port);
    public slots:
        void changeConnectStatus(bool st);

    private:
        QPointer<Server>    m_server;
        QLabel*             m_hostLabel;
        QLabel*             m_portLabel;
        QLineEdit*          m_hostLineEdit;
        QLineEdit*          m_portLineEdit;
        QPushButton*        m_connectButton;
        QPushButton*        m_disconnectButton;
};
#endif