#ifndef _loadmanager_h
#define _loadmanager_h

#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QQueue>

class QTcpSocket;
class QTcpServer;
class QLocalSocket;
class QLocalServer;
class Load;
class Server;

/**
  * This class keeps the interface to qdcl.exe and accepts all buildrequests/finish requests
  * and adds one Load object per request. It also locally keeps track of those requests
  */

class LoadManager : public QObject {
    Q_OBJECT

    public:
        LoadManager(Server *server);
        bool connectStatus() const;
        quint16 tokenNumber() const;

    signals:
        void connectStatusChanged(bool st);
        void disconnected();

    public slots:
        void connectDefault();
        void disconnectServer();

        void sendSlaveInformations();
        void requestJob();
        void finishJob();

        void receiveData();

        void addNewLocalRequest();
    private slots:
        void checkConnectStatus();
        void checkDisconnectStatus();
    private:
        void dumpCurrentStream();

        QPointer<Server>        m_server;
        QTcpSocket*             m_tcpSocket;
        bool                    m_connected;

        QLocalServer*           m_localServer;

        QQueue<QPointer<Load>>  m_loads;
        QQueue<QPointer<Load>>  m_waitLoads;

        int                     m_tokenNumber;
        qint64                  m_blockSize;

        // this is the port to which you can send compileJobs
        int                     m_localPort;
};

#endif