#ifndef _load_h
#define _load_h

#include <QtCore/QByteArray>
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QString>

class QTcpSocket;
class QLocalSocket;
class QSharedMemory;

/** This class takes over the routing between exactly one qdcl.exe instance and its local qdclclient
  * Instances of this class are being kept track by the LoadManager
  */
class Load : public QObject {
    Q_OBJECT
    public:
        Load();
        
        void setRemoteInfo(const QString& address, quint16 port, quint64 id);
        void setLocalSocket(QLocalSocket *socket);
        quint64 id() const;
    signals:
        void jobRequested();
        void jobFinished();
        void terminated();
    public slots:
        void start();
        void readLocalData();
        void readTcpData();
        /** call this slot to tell the qdcl to finish now
          */
//        void finishJob();
        void finishLocalJob();

    private:
        QString         m_address;
        quint16         m_port;

        quint64         m_id;
        quint64         m_size;
        QString         m_key;
        QString         m_params;
        
        quint16         m_exitCode;
        QByteArray      m_content;
        QByteArray      m_errorOutput;

        QPointer<QLocalSocket>  m_localSocket;
        QPointer<QTcpSocket>    m_tcpSocket;
        QPointer<QSharedMemory> m_sharedMemory;

        qint64          m_blockSize;
};
#endif