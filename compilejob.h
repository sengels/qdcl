#ifndef _compilejob_h
#define _compilejob_h

#include <QtCore/QByteArray>
#include <QtCore/QMutex>
#include <QtCore/QPointer>
#include <QtCore/QString>
#include <QtCore/QThread>

class QTcpSocket;
class CompileManager;
class QTemporaryFile;
class QProcess;

/**
  * This class provides the real compile job and is threaded.
  * it gets the data from the requesting Load object and sends back
  * its results.
  */
class CompileJob : public QThread {
    Q_OBJECT
    public:
        CompileJob(CompileManager *man, QTcpSocket *socket = 0);
        void run();
    signals:
        void fileReceived();
    public slots:
        void readData();
        void markFinishedJob();
    private:
        QString                     m_params;
        QString                     m_key;
        QString                     m_tempFileName;
        QByteArray                  m_content;
        QByteArray                  m_errorOutput;
        QByteArray                  m_standardOutput;
        QPointer<QTcpSocket>        m_socket;
        QPointer<CompileManager>    m_manager;
        QPointer<QTemporaryFile>    m_tempFile;
        QPointer<QProcess>          m_process;
        QMutex                      m_mutex;
        qint64                      m_blockSize;
};
#endif