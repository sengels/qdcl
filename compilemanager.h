#ifndef _compilemanager_h
#define _compilemanager_h

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QPointer>

class QTcpServer;
class Server;
class CompileJob;

/**
  * The CompileManager listens on the predefined port for new compilerequests and
  * adds new CompileJobs which execute those jobs.
  */
class CompileManager : public QObject {
    Q_OBJECT
    public:
        CompileManager(Server *server);
        
        QString dataPath() const;
    public slots:
        void addNewJob();

    private:
        QPointer<QTcpServer>         m_tcpServer;
        QPointer<Server>             m_server;
        QList<QPointer<CompileJob>>  m_jobList;
};
#endif